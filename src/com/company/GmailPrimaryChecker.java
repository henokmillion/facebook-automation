package com.company;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class GmailPrimaryChecker {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\Downloads\\Telegram Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://mail.google.com");
        WebDriverWait wait = new WebDriverWait(driver, 120);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("identifierId")));

        WebElement email = driver.findElement(By.id("identifierId"));
        WebElement nextButton = driver.findElement(By.id("identifierNext"));

        email.sendKeys("email");
        nextButton.click();

        boolean loaded = waitForAjax(driver);
        System.out.println(loaded);
        if (loaded) {
            System.out.println(loaded);

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[type='password']")));
            WebElement password = driver.findElements(By.cssSelector("input[type='password']")).get(0);

            password.sendKeys("pass");
            WebElement nextPassButton = driver.findElement(By.id("passwordNext"));
            nextPassButton.click();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(":2l")));
            WebElement primary = driver.findElement(By.id(":2l"));
            WebElement parent = primary.findElement(By.xpath(".."));
            System.out.println(parent.findElement(By.xpath(".//div[@class='aDG']")).getText());
        }

//
//        WebElement password = driver.findElement(By.id("pass"));
//        WebElement loginButton = driver.findElement(By.id("loginbutton"));
//
//        email.sendKeys("+251921069701");
//        password.sendKeys("P@5singword");
//        loginButton.click();
//        WebElement notificationElement = driver.findElement(By.id("notificationsCountValue"));
//        System.out.println("Notification count: " + notificationElement.getAttribute("textContent"));
//
//        System.out.println(driver.getTitle());
    }

    public static boolean waitForAjax(WebDriver driver) {

        WebDriverWait wait = new WebDriverWait(driver, 30);

        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
                } catch (Exception e) {

                    return true;
                }
            }
        };


        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };

        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }
}