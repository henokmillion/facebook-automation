package com.company;

import com.google.gson.JsonObject;
import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ArticleGrabber {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\Downloads\\Telegram Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://addisfortune.net/");
        WebDriverWait wait = new WebDriverWait(driver, 120);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("addisfortune-main")));
        WebElement main = driver.findElement(By.id("addisfortune-main"));
            List<WebElement> divs = main.findElements(By.xpath(".//div[@id='excerpt-post-p-justify']"));
            boolean loaded = false;
        List<String> titles = new ArrayList();
        List<String> excerpts = new ArrayList();

        for(WebElement div : divs) {
            List<WebElement> parents = div.findElements(By.xpath("../..//h3"));
            if (!loaded) {
                for (WebElement parent: parents) {
                    titles.add(parent.getText());
                }
            }
            loaded = true;
            excerpts.add(div.getText());
        }
        System.out.println(titles.size());
        System.out.println(excerpts.size());
        JSONObject obj = new JSONObject();
        for (int i = 0; i < titles.size(); i++) {
            obj.put(titles.get(i), excerpts.get(i));
        }
        try (FileWriter file = new FileWriter("./data.json")) {
            file.write(obj.toJSONString());
            System.out.println("Successfully Copied JSON Object to File...");
            System.out.println("\nJSON Object: " + obj);
        } catch (IOException e) {
            e.printStackTrace();
        }

//
//        WebElement password = driver.findElement(By.id("pass"));
//        WebElement loginButton = driver.findElement(By.id("loginbutton"));
//
//        email.sendKeys("+251921069701");
//        password.sendKeys("P@5singword");
//        loginButton.click();
//        WebElement notificationElement = driver.findElement(By.id("notificationsCountValue"));
//        System.out.println("Notification count: " + notificationElement.getAttribute("textContent"));
//
//        System.out.println(driver.getTitle());
    }

    public static boolean waitForAjax(WebDriver driver) {

        WebDriverWait wait = new WebDriverWait(driver, 30);

        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
                } catch (Exception e) {

                    return true;
                }
            }
        };


        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };

        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }
}