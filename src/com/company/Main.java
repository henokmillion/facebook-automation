package com.company;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Main {

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\Downloads\\Telegram Desktop\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.facebook.com/");

        WebElement email = driver.findElement(By.id("email"));
        WebElement password = driver.findElement(By.id("pass"));
        WebElement loginButton = driver.findElement(By.id("loginbutton"));

        email.sendKeys("email");
        password.sendKeys("password");
        loginButton.click();
        WebElement notificationElement = driver.findElement(By.id("notificationsCountValue"));
        System.out.println("Notification count: " + notificationElement.getAttribute("textContent"));

        System.out.println(driver.getTitle());
    }
}
