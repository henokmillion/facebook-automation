# Selenium automation lab
**Henok Million (ATR/1810/08) - SE(2)**
#### running the node server
To start the server run `node app.js`

#### grabbing the data from `addisfortune`
To grab fresh data from `addisfortune`: compile and run `ArticleGrabber.java`

#### notification check
To start checking notification, edit the email and password, then compile and run `Main.java`