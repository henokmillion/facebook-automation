/**
 * Created by User on 5/25/2018.
 */
const express = require('express')
const app = express()

const server = app.listen(8000, () => {
    const host = server.address().address
    const port = server.address().port
    console.log(`app running on ${host}:${port}`)
})