/**
 * Created by User on 5/25/2018.
 */
const express = require('express')
const app = express()
const fs = require('fs')

app.use(express.static('./'))

function getDictionary() {
    let dictionary = fs.readFileSync(__dirname + "/data.json", 'utf-8')
    return JSON.parse(dictionary.trim())
}

function parseDictionary(dict, word) {
    return new Promise((succeed, fail) => {
            if(dict.hasOwnProperty(word)) {
        succeed(dict[word])
    } else {
        fail(new Error(`"${word}" not found`))
    }
})
}

app.get('/api/v1/dictionary', (req, res) => {
        res.status(200).json({
            status: 200,
            data: getDictionary()
        })
    
})
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/articles.html')
})



const server = app.listen(8000, () => {
    const host = server.address().address
    const port = server.address().port
    console.log(`app running on ${host}:${port}`)
})